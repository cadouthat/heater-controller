#include <Adafruit_MCP9808.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <eeprom-wifi.h>

// Default MCP9808 breakout address, no address pins connected.
#define TEMP_SENSOR_I2C_ADDR 0x18
// The most precise resolution (0.0625 deg), but longest sample time (250ms).
#define TEMP_SENSOR_RESOLUTION_MODE 3
#define IR_OUTPUT_PIN 14
#define IR_OUTPUT_INACTIVE LOW
#define IR_FREQ 38000
#define SERIAL_BAUD 115200

#define SERVER_NAME "heat.lan"
#define SERVER_PORT 8089

#define COMMAND_NOP 0
#define COMMAND_TOGGLE_POWER 100

namespace {

constexpr int kPWMUniform = 128;
constexpr int kPWMOff = 0;

constexpr int kSignalLow_us = 563;
constexpr int kSignalHigh_us = 1688;

constexpr int kPowerSequence_us[] = {
  9000, 4500,
  kSignalLow_us, kSignalHigh_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us,
  kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us,
  kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalHigh_us, kSignalLow_us,
  kSignalHigh_us, kSignalLow_us, kSignalHigh_us, kSignalLow_us, kSignalHigh_us, kSignalLow_us, kSignalHigh_us,
  kSignalLow_us, kSignalHigh_us, kSignalLow_us, kSignalHigh_us, kSignalLow_us, kSignalHigh_us, kSignalLow_us,
  kSignalHigh_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us,
  kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us,
  kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalHigh_us, kSignalLow_us, kSignalHigh_us,
  kSignalLow_us, kSignalHigh_us, kSignalLow_us, kSignalHigh_us, kSignalLow_us, kSignalHigh_us, kSignalLow_us,
  kSignalHigh_us, kSignalLow_us, kSignalHigh_us, kSignalLow_us, kSignalHigh_us, kSignalLow_us, kSignalLow_us,
  kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us,
  kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us, kSignalLow_us,
  kSignalLow_us, kSignalHigh_us, kSignalLow_us, kSignalHigh_us, kSignalLow_us, kSignalHigh_us, kSignalLow_us,
  kSignalHigh_us, kSignalLow_us, kSignalHigh_us, kSignalLow_us, kSignalHigh_us, kSignalLow_us
};

} // namespace

void delayMicrosSinceLast(unsigned long us) {
  static unsigned long target_us = 0;
  unsigned long now_us = micros();
  if (!us) {
    target_us = now_us;
  } else {
    target_us += us;
  }
  if (target_us > now_us) {
    delayMicroseconds(target_us - now_us);
  }
}

void sendPowerSequence() {
  noInterrupts();

  bool signal_active = true;
  delayMicrosSinceLast(0);
  for (int i = 0; i < sizeof(kPowerSequence_us) / sizeof(int); i++) {
    if (signal_active) {
      analogWrite(IR_OUTPUT_PIN, kPWMUniform);
    } else {
      analogWrite(IR_OUTPUT_PIN, kPWMOff);
      digitalWrite(IR_OUTPUT_PIN, IR_OUTPUT_INACTIVE);
    }
    signal_active = !signal_active;
    delayMicrosSinceLast(kPowerSequence_us[i]);
  }

  analogWrite(IR_OUTPUT_PIN, kPWMOff);
  digitalWrite(IR_OUTPUT_PIN, IR_OUTPUT_INACTIVE);

  interrupts();
}

Adafruit_MCP9808 temp_sensor;
WiFiClient client;

float sampleTempF(int samples = 10, int interval_ms = 1000) {
  // Resume temperature sampling.
  temp_sensor.wake();
  float total = 0;
  for (int i = 0; i < samples; i++) {
    total += temp_sensor.readTempF();
    delay(interval_ms);
  }
  // Stop temperature sampling to reduce power consumption.
  temp_sensor.shutdown();
  return total / samples;
}

void setup() {
  // The built-in LED is blinked to indicate WiFi configuration is needed.
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  // Make sure the infrared LED is off for now, but configure frequency
  // that will be used for PWM later.
  pinMode(IR_OUTPUT_PIN, OUTPUT);
  digitalWrite(IR_OUTPUT_PIN, IR_OUTPUT_INACTIVE);
  analogWriteFreq(IR_FREQ);

  // Serial is used for initial WiFi configuration.
  Serial.begin(SERIAL_BAUD);
  delay(500);

  if (!temp_sensor.begin(TEMP_SENSOR_I2C_ADDR)) {
    Serial.println(F("Couldn't find MCP9808!"));
    while(1) delay(100);
  }
  temp_sensor.setResolution(TEMP_SENSOR_RESOLUTION_MODE);

  EEPROM.begin(kWiFiConfigSize);
  // Block until WiFi is connected.
  while (!connectToWiFi()) {
    digitalWrite(LED_BUILTIN, LOW);
    delay(500);
    digitalWrite(LED_BUILTIN, HIGH);
    // Prompt for new configuration via serial.
    if (!configureWiFi()) {
      Serial.println(F("WiFi configuration failed!"));
      delay(1000);
    }
  }
}

#define MIN_SERIALIZED_TEMP 0.0
#define MAX_SERIALIZED_TEMP 100.0
uint16_t serializeTemp(float temp) {
  float scale = (temp - MIN_SERIALIZED_TEMP) / (MAX_SERIALIZED_TEMP - MIN_SERIALIZED_TEMP);
  if (scale < 0) scale = 0;
  if (scale > 1) scale = 1;
  return scale * UINT16_MAX;
}

void doCommand(uint8_t command) {
  if (command == COMMAND_TOGGLE_POWER) {
    sendPowerSequence();
  }
}

void loop() {
	if (!client.connected() && !client.connect(SERVER_NAME, SERVER_PORT)) {
		Serial.println(F("Failed to connect!"));
    delay(5000);
    return;
	}

  float temp = sampleTempF();

  uint16_t serialized = serializeTemp(temp);
  uint8_t request[] = { serialized >> 8, serialized & 0xFF };
  if (client.write(request, sizeof(request)) != sizeof(request)) {
    Serial.println("TCP write failed");
    client.stop();
    return;
  }

  // Wait for response
  delay(1000);

  while (client.available() > 0) {
    doCommand(client.read());
  }

  delay(60000);
}
