const { PubSub } = require('@google-cloud/pubsub');
const tcpDebug = require('debug')('heater-server:tcp');
const smsDebug = require('debug')('heater-server:sms');
const express = require('express');
const fs = require('fs');
const path = require('path');
const net = require('net');
const twilio = require('twilio');

const DEFAULT_TCP_PORT = 8089;

const pubSubSubscriptionName = process.env.GOOGLE_PUBSUB_SUBSCRIPTION_NAME;
if (!pubSubSubscriptionName) {
  throw new Error('GOOGLE_PUBSUB_SUBSCRIPTION_NAME is required');
}

const twilioConfigPath = process.env.TWILIO_CONFIG_PATH;
if (!twilioConfigPath) {
  throw new Error('TWILIO_CONFIG_PATH is required');
}
const twilioConfig = JSON.parse(fs.readFileSync(twilioConfigPath));

const AlertType = {
  SERVER_STARTED: 'SERVER_STARTED',
  ENABLED: 'ENABLED',
  SENSOR_TIMEOUT: 'SENSOR_TIMEOUT',
  TEMPERATURE: 'TEMPERATURE',
  TOGGLE_ERROR: 'TOGGLE_ERROR',
  PUBSUB_ERROR: 'PUBSUB_ERROR',
};

const HVAC_COOLING = 'COOLING';
const HVAC_HEATING = 'HEATING';

const COMMAND_NOP = 0;
const COMMAND_TOGGLE_POWER = 100;

const UINT16_MAX = (1 << 16) - 1;
const MIN_SERIALIZED_TEMP = 0;
const MAX_SERIALIZED_TEMP = 100;
function deserializeTemp(serialized) {
  return MIN_SERIALIZED_TEMP +
    (serialized / UINT16_MAX) * (MAX_SERIALIZED_TEMP - MIN_SERIALIZED_TEMP);
}

function getRoomName() {
  return process.env.ROOM_NAME || 'Room';
}

const config = {
  isEnabled: false,
  targetTempInF: 72,
  heatingLatencyMs: 8 * 60 * 1000,
  maxDeltaMs: 60 * 60 * 1000,
  comfortThresholdInF: 1.0,
  coolingLockoutDurationMs: 60 * 60 * 1000,
  alertsEnabled: true,
  alertThrottleMs: 10 * 60 * 1000,
  alertSensorTimeoutMs: 10 * 60 * 1000,
  alertHighTempInF: 80.0,
  alertLowTempInF: 65.0,
  lastAlertMsByType: {}, // Persisted in config to respect throttling across server reboots
  rollingWindowSize: 5,
  sampleLogLength: 2 * 24 * 60, // ~2 days
  emeterLatencyMs: 20 * 1000,
  heatingPowerThresholdWatts: 100,
  toggleRetryAlertThreshold: 0,
};

function updateConfig(changes) {
  for (const [key, val] of Object.entries(changes)) {
    if (!config.hasOwnProperty(key) || typeof config[key] != typeof val) {
      throw new Error(`invalid config ${key}`);
    }
  }

  for (const [key, val] of Object.entries(changes)) {
    config[key] = val;
  }

  storeConfig();
}

function storeConfig() {
  if (!process.env.CONFIG_PATH) {
    return;
  }
  
  fs.writeFileSync(process.env.CONFIG_PATH, JSON.stringify(config, null, 2));
}

function loadConfig() {
  if (!process.env.CONFIG_PATH || !fs.existsSync(process.env.CONFIG_PATH)) {
    return;
  }

  const savedConfig = JSON.parse(fs.readFileSync(process.env.CONFIG_PATH));

  updateConfig(savedConfig);
}

const history = {
  serverStartMs: new Date().getTime(),
  isHeating: false,
  hvacStatus: '',
  hvacCoolingEndTimestampMs: 0,
  hvacHeatingEndTimestampMs: 0,
  sampleLog: [],
  smsErrorCount: 0,
  hvacTimestamp: new Date(0),
  emergencyTogglePending: false,
  alertSnoozeUntilMs: 0,
  emeterTimestampMs: 0,
  emeterPowerWatts: 0,
  emeterLastAboveThresholdMs: 0,
  lastToggleMs: 0,
  toggleRetryCount: 0,
};

function getLastSample() {
  if (!history.sampleLog.length) {
    return undefined;
  }

  return history.sampleLog[history.sampleLog.length - 1];
}

let twilioClient = undefined;
function getTwilioClient() {
  if (!twilioClient) {
    twilioClient = twilio(twilioConfig.accountSid, twilioConfig.authToken);
  }
  return twilioClient;
}

async function sendAlertSms(type, message) {
  const nowMs = new Date().getTime();
  if (!config.alertsEnabled) {
    return;
  }

  if (nowMs < history.alertSnoozeUntilMs ||
      nowMs - (config.lastAlertMsByType[type] || 0) < config.alertThrottleMs) {
    return;
  }
  config.lastAlertMsByType[type] = nowMs;
  storeConfig();

  if (!twilioConfig.heaterAlertRecipients) {
    console.error(`No recipients for alert: "${message}"`);
    return;
  }

  for (const recipient of twilioConfig.heaterAlertRecipients) {
    try {
      const response = await getTwilioClient().messages.create({
        body: message,
        to: recipient,
        from: twilioConfig.fromNumber,
      });
      smsDebug('SMS send success', response.sid);
    } catch (err) {
      history.smsErrorCount++;
      console.error('SMS send error', err);
    }
  }
}

function checkAlerts() {
  const nowMs = new Date().getTime();

  if (nowMs - history.serverStartMs > config.alertSensorTimeoutMs) {
    const lastSample = getLastSample();
    if (!lastSample || nowMs - lastSample.timestampMs > config.alertSensorTimeoutMs) {
      sendAlertSms(AlertType.SENSOR_TIMEOUT, `${getRoomName()} sensor is offline`);
      return;
    }

    if (nowMs - history.emeterTimestampMs > config.alertSensorTimeoutMs) {
      sendAlertSms(AlertType.SENSOR_TIMEOUT, `${getRoomName()} emeter is offline`);
      return;
    }
  }
}

function checkTempAlerts(tempInF) {
  if (tempInF > config.alertHighTempInF || tempInF < config.alertLowTempInF) {
    sendAlertSms(AlertType.TEMPERATURE, `${getRoomName()} temperature is ${tempInF.toFixed(1)} F`);
    return;
  }
}

function recordSample(rawTempInF) {
  while (history.sampleLog.length >= config.sampleLogLength) {
    history.sampleLog.shift();
  }

  const rollingWindow = [rawTempInF];
  for (let i = 1; i < config.rollingWindowSize; i++) {
    if (history.sampleLog.length - i < 0) {
      break;
    }
    rollingWindow.push(history.sampleLog[history.sampleLog.length - i].rawTempInF);
  }
  const rollingAvg = rollingWindow.reduce((a, b) => a + b, 0) / rollingWindow.length;

  const nowMs = new Date().getTime();
  const sample = {
    timestampMs: nowMs,
    rawTempInF,
    tempInF: rollingAvg,
    isHeating: history.isHeating,
    emeterPowerWatts: history.emeterPowerWatts,
    emeterSampleAge: nowMs - history.emeterTimestampMs,
  };

  history.sampleLog.push(sample);

  return sample;
}

function getHistoricalTempInF(timestampMs) {
  let prevSample = undefined;
  let nextSample = undefined;
  for (let i = history.sampleLog.length - 1; i >= 0; i--) {
    if (history.sampleLog[i].timestampMs < timestampMs) {
      prevSample = history.sampleLog[i];
      break;
    }
    nextSample = history.sampleLog[i];
  }

  if (!prevSample || !nextSample) {
    return undefined;
  }

  const interp =
    (timestampMs - prevSample.timestampMs) / (nextSample.timestampMs - prevSample.timestampMs);
  return prevSample.tempInF + interp * (nextSample.tempInF - prevSample.tempInF);
}

function shouldRetryToggle() {
  // Wait for emeter to settle after toggle
  if (history.emeterTimestampMs - history.lastToggleMs < config.emeterLatencyMs) {
    return false;
  }

  let errorDetected = false;
  if (history.isHeating) {
    // Heater may enter rest intervals, so only verify that initial heating was observed since toggle
    errorDetected = history.emeterLastAboveThresholdMs < history.lastToggleMs;
  } else {
    errorDetected = history.emeterPowerWatts > config.heatingPowerThresholdWatts;
  }

  if (errorDetected) {
    if (history.toggleRetryCount >= config.toggleRetryAlertThreshold) {
      sendAlertSms(AlertType.TOGGLE_ERROR, `${getRoomName()} heater detected a toggle error`);
    }

    history.toggleRetryCount++;
    return true;
  }
  
  history.toggleRetryCount = 0;
  return false;
}

function shouldBeHeating(tempInF, wasHeating) {
  if (!config.isEnabled) {
    return false;
  }

  const nowMs = new Date().getTime();

  if (history.hvacStatus == HVAC_COOLING ||
      nowMs - history.hvacCoolingEndTimestampMs < config.coolingLockoutDurationMs) {
    return false;
  }
  if (history.hvacStatus == HVAC_HEATING) {
    return false;
  }

  let predictedTempInF = tempInF;

  const referenceMs = Math.max(
    history.lastToggleMs + config.heatingLatencyMs,
    history.hvacHeatingEndTimestampMs,
    history.hvacCoolingEndTimestampMs,
    nowMs - config.maxDeltaMs,
  );
  const referenceTempInF = getHistoricalTempInF(referenceMs);
  if (referenceTempInF !== undefined && nowMs - referenceMs >= config.heatingLatencyMs) {
    const deltaPerMs = (tempInF - referenceTempInF) / (nowMs - referenceMs);
    predictedTempInF += deltaPerMs * config.heatingLatencyMs;
  }

  if (wasHeating) {
    return predictedTempInF < config.targetTempInF + config.comfortThresholdInF;
  }

  return predictedTempInF <= config.targetTempInF - config.comfortThresholdInF;
}

const tcpServer = net.createServer((socket) => {
  tcpDebug('Socket connected');

  socket.on('data', (data) => {
    if (data.length != 2) {
      tcpDebug('Bad message');
      socket.destroy();
      return;
    }

    const serialized = data.readUint16BE();
    const rawTempInF = deserializeTemp(serialized);
    const sample = recordSample(rawTempInF);

    let command = COMMAND_NOP;
    if (history.emergencyTogglePending) {
      history.emergencyTogglePending = false;
      command = COMMAND_TOGGLE_POWER;
    } else if (shouldRetryToggle()) {
      command = COMMAND_TOGGLE_POWER;
    } else if (history.isHeating != shouldBeHeating(sample.tempInF, history.isHeating)) {
      history.isHeating = !history.isHeating;
      command = COMMAND_TOGGLE_POWER;
    }

    if (command == COMMAND_TOGGLE_POWER) {
      history.lastToggleMs = new Date().getTime();
    }
    socket.write(new Uint8Array([command]));

    sample.isHeating = history.isHeating;

    checkTempAlerts(sample.tempInF);
  });
  
  socket.on('close', () => {
    tcpDebug('Connection closed');
  });

  socket.on('error', (err) => {
    tcpDebug(err);
  });
});

tcpServer.on('error', (err) => {
  console.error(err);
  process.exit(1);
});

tcpServer.on('listening', () => {
  tcpDebug('Listening on', tcpServer.address());
})

tcpServer.listen(parseInt(process.env.TCP_PORT) || DEFAULT_TCP_PORT, '0.0.0.0');

const app = express();

app.use(express.json());

app.use(express.static(path.join(__dirname, 'public')));
app.use('/', express.static(path.join(__dirname, 'public/index.html')));

app.get('/history', (_req, res) => {
  res.json(history);
});

app.get('/config', (_req, res) => {
  res.json(config);
});

app.post('/config', (req, res) => {
  const wasEnabled = config.isEnabled;
  updateConfig(req.body);

  if (config.isEnabled && !wasEnabled) {
    sendAlertSms(AlertType.ENABLED, `${getRoomName()} heater is now enabled`);
  }

  res.json(config);
});

app.post('/snoozeAlerts', (req, res) => {
  if (!(req.body.snoozeUntilMs >= 0)) {
    throw new Error('requires snoozeUntilMs');
  }

  history.alertSnoozeUntilMs = req.body.snoozeUntilMs;

  res.json(history);
});

app.post('/emergencyToggle', (_req, res) => {
  if (history.emergencyTogglePending) {
    throw new Error('already in progress');
  }

  history.emergencyTogglePending = true;

  res.json({});
});

app.post('/emeter', (req, res) => {
  const power = Number(req.body.power);
  if (isNaN(power)) {
    throw new Error('power must be a number');
  }

  const nowMs = new Date().getTime();
  history.emeterPowerWatts = power;
  history.emeterTimestampMs = nowMs;

  if (power > config.heatingPowerThresholdWatts) {
    history.emeterLastAboveThresholdMs = nowMs;
  }

  res.json({});
});

const pubSubClient = new PubSub();
const subscription = pubSubClient.subscription(pubSubSubscriptionName);

subscription.on('message', (message) => {
  message.ack();

  const data = JSON.parse(message.data.toString());
  const hvacStatus = data?.resourceUpdate?.traits?.['sdm.devices.traits.ThermostatHvac']?.status;

  const timestamp = new Date(data.timestamp);
  if (hvacStatus && timestamp > history.hvacTimestamp) {
    history.hvacTimestamp = timestamp;

    if (history.hvacStatus == HVAC_COOLING && hvacStatus != HVAC_COOLING) {
      history.hvacCoolingEndTimestampMs = new Date().getTime();
    }

    if (history.hvacStatus == HVAC_HEATING && hvacStatus != HVAC_HEATING) {
      history.hvacHeatingEndTimestampMs = new Date().getTime();
    }

    history.hvacStatus = hvacStatus;
  }
});

subscription.on('error', (err) => {
  console.error('PubSub error', err);

  sendAlertSms(AlertType.PUBSUB_ERROR, `PubSub error for ${getRoomName()} heater`);

  subscription.close();
});

subscription.on('close', () => {
  console.error('PubSub closed');

  setTimeout(() => subscription.open(), 60000);
});

loadConfig();

sendAlertSms(AlertType.SERVER_STARTED, `${getRoomName()} heater server started`);

setInterval(checkAlerts, 10000);

module.exports = app;
