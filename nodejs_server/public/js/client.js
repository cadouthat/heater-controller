function getHistory() {
  return $.ajax({
    url: '/history',
    method: 'GET',
    dataType: 'json',
  });
}

function getConfig() {
  return $.ajax({
    url: '/config',
    method: 'GET',
    dataType: 'json',
  });
}

function updateConfig(request) {
  return $.ajax({
    url: '/config',
    method: 'POST',
    data: JSON.stringify(request),
    contentType: 'application/json',
    dataType: 'json',
  });
}

function requestSnoozeAlerts(snoozeUntilMs) {
  return $.ajax({
    url: '/snoozeAlerts',
    method: 'POST',
    data: JSON.stringify({ snoozeUntilMs }),
    contentType: 'application/json',
    dataType: 'json',
  });
}

function requestEmergencyToggle() {
  return $.ajax({
    url: '/emergencyToggle',
    method: 'POST',
    data: JSON.stringify({}),
    contentType: 'application/json',
    dataType: 'json',
  });
}
